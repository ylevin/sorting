package ru.ylevin.sorting

import java.io.File

object App {
    private const val DEFAULT_CHUNK_SIZE = 1000 * 1024 * 1024 // 1g

    @JvmStatic
    fun main(args: Array<String>) {
        val srcFile = System.getProperty("srcFile")?.let{ File(it) }
            ?: throw Exception("Please specify source file: -DsrcFile=<filepath>")
        val dstFile = System.getProperty("dstFile")?.let { File(it) } ?: srcFile
        ChunkMergeSorter(System.getProperty("chunkSize")?.toInt() ?: DEFAULT_CHUNK_SIZE)
            .sort(srcFile, dstFile)
    }
}