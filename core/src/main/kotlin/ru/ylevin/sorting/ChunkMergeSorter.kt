package ru.ylevin.sorting

import java.io.Closeable
import java.io.File

class ChunkMergeSorter(chunkSize: Int) {
    private val maxCount = chunkSize / 4
    private val maxSize = chunkSize - maxCount

    init {
        require(maxCount > 0)
        require(maxSize > 0)
    }

    private val buffer = ArrayList<String>(maxCount)

    private fun readChunk(iterator: Iterator<String>): MutableList<String> {
        var size = 0

        while (iterator.hasNext() && buffer.size < maxCount && size < maxSize) {
            val nextLine = iterator.next()
            size += nextLine.length
            buffer += nextLine
        }

        return buffer
    }

    private fun writeChunk(chunk: List<String>, dstFile: File) {
        dstFile.bufferedWriter().use { writer ->
            var first = true
            chunk.forEach { line ->
                if (first)
                    first = false
                else
                    writer.appendln()

                writer.append(line)
            }

            writer.flush()
        }
    }

    private fun mergeAllChunk(chunkFiles: List<File>, dstFile: File) {
        var files = chunkFiles
        while (files.size > 2) {
            files = mergeMultipleChunks(files)
        }

        when {
            files.size == 2 -> mergeTwoChunks(files[0], files[1], dstFile)
            files.size == 1 -> {
                if (dstFile.exists()) dstFile.delete()
                files[0].renameTo(dstFile)
            }
            else -> {
                if (dstFile.exists()) dstFile.delete()
                dstFile.createNewFile()
            }
        }
    }

    private fun mergeMultipleChunks(files: List<File>): List<File> {
        val newFiles = mutableListOf<File>()
        files.windowed(2, 2, true).map { f ->
            newFiles += if (f.size == 1) f[0] else {
                createTempFile().also { dstFile ->
                    mergeTwoChunks(f[0], f[1], dstFile)
                    f[0].delete()
                    f[1].delete()
                }
            }
        }
        return newFiles
    }

    private fun mergeTwoChunks(file1: File, file2: File, dstFile: File) {
        dstFile.bufferedWriter().use { writer ->

            listOf(file1, file2).map { it.bufferedReader() }.closable().use { list ->
                val readers = list.toMutableList()

                val lines: MutableList<String> =
                    readers.mapTo(ArrayList(readers.size)) { it.readLine() }

                while (true) {
                    val minIndex = if (lines[0] < lines[1]) 0 else 1
                    writer.append(lines[minIndex])
                    writer.appendln()

                    val nextLine: String? = readers[minIndex].readLine()
                    if (nextLine == null) {
                        readers.removeAt(minIndex)
                        lines.removeAt(minIndex)
                        break
                    } else {
                        lines[minIndex] = nextLine
                    }
                }

                val reader = readers[0]
                var line: String? = lines[0]

                while (line != null) {
                    writer.append(line)
                    line = reader.readLine()

                    if (line != null) {
                        writer.appendln()
                    }
                }
            }

            writer.flush()
        }
    }


    fun sort(srcFile: File, dstFile: File) {
        if (!srcFile.exists()) {
            throw IllegalArgumentException("Not found file $srcFile")
        }

        if (srcFile.isDirectory) {
            throw IllegalArgumentException("This is directory $srcFile")
        }

        val dir = createTempDir()
        try {
            val files = mutableListOf<File>()
            srcFile.useLines { lines ->
                val iterator = lines.iterator()
                while (iterator.hasNext()) {
                    files += createTempFile(directory = dir).also { file ->
                        val chunk = readChunk(iterator)
                        chunk.sort()
                        writeChunk(chunk, file)
                        buffer.clear()
                    }
                }
                mergeAllChunk(files, dstFile)
            }
        } finally {
            dir.deleteRecursively()
        }
    }

    private fun <E : Closeable?> List<E>.closable() = ClosableList(this)

    private class ClosableList<E : Closeable?>(list: List<E>) : List<E> by list, Closeable {
        override fun close() {
            forEach { it?.close() }
        }
    }
}