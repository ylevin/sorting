package ru.ylevin.sorting

import kotlin.system.measureTimeMillis
import kotlin.test.Ignore
import kotlin.test.Test
import kotlin.test.assertTrue

class GeneratedTest {
    private fun test(sorter: ChunkMergeSorter, generator: Generator) {
        val srcFile = createTempFile()
        val dstFile = createTempFile()
        try {
            println("Generating...")
            generator.generate(srcFile)
            println("File size = ${srcFile.length()}")
            println("Sorting...")
            val time = measureTimeMillis {
                sorter.sort(srcFile, dstFile)
            }
            println("Time = ${time}ms")
            println("Checking...")
            assertTrue(Checker.check(dstFile))
        } finally {
            srcFile.delete()
            dstFile.delete()
        }
    }

    @Test
    fun test10mb() {
        test(ChunkMergeSorter(1_000_000), Generator(100_000, 0..200, 111))
    }

    @Test
    fun test9mb() {
        test(ChunkMergeSorter(1_000_000), Generator(90_000, 0..200, 111))
    }

    @Test
    fun test6mb() {
        test(ChunkMergeSorter(1_000_000), Generator(60_000, 0..200, 111))
    }

    @Test
    fun test3mb() {
        test(ChunkMergeSorter(1_000_000), Generator(30_000, 0..200, 111))
    }

    @Test
    fun test2mb() {
        test(ChunkMergeSorter(1_000_000), Generator(20_000, 0..200, 111))
    }

    @Test
    @Ignore("This is large test. Run it manually")
    fun test5gb() {
        test(ChunkMergeSorter(1_000_000_000), Generator(5_000_000, 0..2000, 111))
    }
}