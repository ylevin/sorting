package ru.ylevin.sorting

import kotlin.test.Test
import kotlin.test.assertEquals

class CorrectnessTest {
    private fun test(sorter: ChunkMergeSorter, expected: String, source: String) {
        val srcFile = createTempFile()
        val dstFile = createTempFile()
        try {
            srcFile.writeText(source)
            sorter.sort(srcFile, dstFile)
            assertEquals(expected, dstFile.readText())
        } finally {
            srcFile.delete()
            dstFile.delete()
        }
    }

    private fun testLines(sorter: ChunkMergeSorter, lines: List<String>) {
        val sortedLines = lines.sorted()
        test(sorter,
            expected = sortedLines.joinToString("\n"),
            source = lines.joinToString("\n")
        )
    }

    @Test
    fun testEmptyFile() {
        test(ChunkMergeSorter(20), "", "")
        test(ChunkMergeSorter(10000), "", "")
    }

    @Test
    fun testSingleLine() {
        test(ChunkMergeSorter(20), "abcdefg", "abcdefg")
    }

    @Test
    fun testTwoLines() {
        test(ChunkMergeSorter(20), "abcdefg\nhij", "abcdefg\nhij")
    }

    @Test
    fun testTwoLinesInverted() {
        test(ChunkMergeSorter(20), "abcdefg\nhij", "hij\nabcdefg")
    }

    @Test
    fun testManyFiles() {
        testLines(ChunkMergeSorter(20),
            listOf("abc1553", "aaaaaaa", "cdefggg", "gggggg",
                "ffff", "abdedec", "zde", "dffg",
                "1234", "4321", "5556", "18120", "zdd", "zzaz", "abbbc")
        )
    }

    @Test
    fun testInPlace() {
        val sorter = ChunkMergeSorter(20)
        val srcFile = createTempFile()
        try {
            srcFile.writeText("zzz\nabc\ncde\ndddfd")
            sorter.sort(srcFile, srcFile)
            assertEquals("abc\ncde\ndddfd\nzzz", srcFile.readText())
        } finally {
            srcFile.delete()
        }
    }
}