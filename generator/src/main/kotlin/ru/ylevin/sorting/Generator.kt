package ru.ylevin.sorting

import java.io.File
import kotlin.random.Random
import kotlin.random.nextInt
import kotlin.text.StringBuilder

class Generator(private val linesCount: Int,
                private val lineLength: IntRange,
                seed: Long = System.currentTimeMillis()) {

    private val chars = StringBuilder().also { builder ->
        ('0'..'9').forEach { builder.append(it) }
        ('a'..'z').forEach { builder.append(it) }
        ('A'..'Z').forEach { builder.append(it) }
    }.toString()

    private val random = Random(seed)

    fun generate(file: File) {
        file.bufferedWriter().use { writer ->
            val builder = StringBuilder(lineLength.endInclusive)
            for (i in 1..linesCount) {
                builder.clear()
                val length = random.nextInt(lineLength)
                for (j in 1..length) {
                    builder.append(chars.random(random))
                }
                writer.appendln(builder.toString())
            }
        }
    }
}