package ru.ylevin.sorting

import java.io.File

object Checker {
    fun check(file: File): Boolean {
        return file.useLines { lines ->
            lines.windowed(2, 1).all { pair ->
                pair[0] <= pair[1]
            }
        }
    }
}