package ru.ylevin.sorting

import org.junit.Test
import kotlin.test.assertTrue

class GeneratorTest {
    private fun test(linesCount: Int, lineLength: IntRange, seed: Long) {
        val srcFile = createTempFile()
        try {
            Generator(linesCount, lineLength, seed).generate(srcFile)
            var count = 0
            srcFile.forEachLine { line ->
                count++
                assertTrue(line.length in lineLength)
            }
        } finally {
            srcFile.delete()
        }
    }

    @Test
    fun testGeneratorWithEmptyLines() {
        test(100, 0..1000, 20)
    }

    @Test
    fun testGeneratorFixedLines() {
        test(100, 1000..1000, 20)
    }
}