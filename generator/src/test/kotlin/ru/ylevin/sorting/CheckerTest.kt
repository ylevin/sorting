package ru.ylevin.sorting

import org.junit.Test
import kotlin.test.assertEquals

class CheckerTest {
    private fun test(text: String, expected: Boolean) {
        val file = createTempFile()
        try {
            file.writeText(text)
            assertEquals(expected, Checker.check(file))
        } finally {
            file.delete()
        }
    }

    @Test
    fun testEmptyFile() {
        test("", true)
    }

    @Test
    fun testNewLineSym() {
        test("\n", true)
    }


    @Test
    fun testSingleLine() {
        test("abc", true)
    }

    @Test
    fun testTwoLines() {
        test("abc\ncde", true)
    }

    @Test
    fun testEqualLines() {
        test("abc\ncde\ncde", true)
    }

    @Test
    fun testFailed() {
        test("zzz\ncde\ncde", false)
    }

    @Test
    fun testEmptyLines() {
        test("aaa\n\n", false)
        test("\n\naaa", true)
    }
}